const express = require("express");
const port = 2723;
const app = express();

app.get("/", (req,res)=> res.send("welcome"));

app.get("/add", (req,res)=>{
    try{
        const sum= parseInt(req.query.a)+parseInt(req.query.b);
        res.send(sum.toString())
    } catch(e){
        res.sendStatus(500);
    }
});

if (process.env.NODE_ENV === "test") {
    module.exports = app;
}

if (!module.parent) {
    app.listen(port, () => console.log(`Server running at localhost:${port}`));
}


app.listen(port, ()=> console.log(`server running at the localhost:${port}`));